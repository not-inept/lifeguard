package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// The struct representing lifeguard's configuration
type lifeGuardConfig struct {
	Token     string `json:"token"`
	WatchPath string `json:"watch_path"`
}

// Read the provided configuration file
func loadConfig(filePath string) lifeGuardConfig {
	logrus.Info("Loading lifeguard configuration.")
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		logrus.Fatalf("Error: %v", err)
	}
	config := lifeGuardConfig{}
	err = json.Unmarshal(data, &config)
	if err != nil {
		logrus.Fatalf("Error: %v", err)
	}
	return config
}

func main() {
	logrus.Info("Starting up Lifeguard.")
	config := loadConfig("lifeguard.json")

	logrus.Info("Loading watches...")
	loaded, err := LoadWatches(config.WatchPath)
	if err != nil {
		logrus.Error("Failed to load watches!")
	}
	logrus.Infof("Loaded %d watches.", loaded)

	discord, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		logrus.Fatal("Error creating Discord session,", err)
		return
	}

	discord.AddHandler(MessageCreate)

	err = discord.Open()
	if err != nil {
		logrus.Fatal("Error opening connection,", err)
		return
	}

	go WatchWorker(discord, 5*time.Minute)

	// Wait here until CTRL-C or other term signal is received.
	logrus.Infof("Lifeguard is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	discord.Close()

	logrus.Info("Storing watches...")
	saved, err := SaveWatches(config.WatchPath)
	if err != nil {
		logrus.Fatal("Failed to save watches!")
	}
	logrus.Infof("Saved %d watches.", saved)
}
