# Lifeguard

Lifeguard is a discord bot that keeps an eye on your mining pool so you don't have to worry about how your workers are doing.

Right now it's mostly an MVP for watching a group of miners I have with luxor.tech, but I'm looking to add more to this over time. 

**WARNING**: Right now, watches are only saved if I close out the application manually.

I just added a quick dump to json for now, but I may do more in the future.
To add me to your server, checkout: [https://discordapp.com/oauth2/authorize?client_id=524767544750374942&permissions=0&scope=bot](https://discordapp.com/oauth2/authorize?client_id=524767544750374942&permissions=0&scope=bot)

# Usage
You'll need to run this in a directory with `lifeguard.json`, which will contain your bot token. `lifeguard.example.json` shows what that file should look like.

# Global Events Tracked
- miner count changes by percentage
- hashrate changes by percentage
- block is found

## Planned
- api is non-responsive

# User Specific Events Tracked
- miner count changes from expected 
- list specific workers that have been added/removed
- hashrate changes by percentage
- block is found (for fun)
## Planned
- stale share count up by percentage
- invalid share count up by percentage
- valid share count decreases by percentage
- last share time is stale


# Commands

## !lifeguard
This will give a quick message about the bot.

## !help
This will give a quick message about the bot and indicate you should use `!lifeguard help` for more.

## !lifeguard list
TODO: This will list all active watches you have and their configuration. 

## !lifeguard register-luxor <coin>
This allows you to register a watch over the luxor pool for a coin which will be triggered on the events listed above. There is intent to allow you to configure the thresholds the watches trigger on later on.

TODO: If used in public channels, you must be a server admin to use this command. Try using it in a direct message!

## !lifeguard register-luxor <coin> <wallet_addr>
This allows you to register a watch over a specifc wallet address which will be triggered on the events listed above. There is intent to allow you to configure the thresholds the watches trigger on later on.

TODO: If used in public channels, you must be a server admin to use this command (er... you will be, but not yet). Try using it in a direct message!