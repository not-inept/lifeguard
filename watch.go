package main

import (
	"encoding/json"
	"io/ioutil"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/not-inept/lifeguard/luxor"
)

// Sync map for watches
var watches sync.Map

var failedUserRequests int
var failedStatsRequests int

// Watch contains what we're watching on the pool API
type Watch struct {
	OwnerID    string
	ChannelID  string
	Pool       string
	Coin       string
	Wallet     string
	BaseValues string
}

// SaveWatches saves the sync.Map to disk, as sync.Map itself
// isn't able to be marshalled
func SaveWatches(filePath string) (int, error) {
	tmpMap := make(map[string]*Watch)
	var savedWatches int
	watches.Range(func(k, v interface{}) bool {
		tmpMap[k.(string)] = v.(*Watch)
		savedWatches++
		return true
	})
	mapBytes, err := json.Marshal(tmpMap)
	if err != nil {
		logrus.Error(err)
		return 0, err
		// TODO should probably catch this, as otherwise everything is lost
	}
	err = ioutil.WriteFile(filePath, mapBytes, 0777)
	if err != nil {
		logrus.Error(err)
		return 0, err
		// TODO should probably catch this, as otherwise everything is lost
	}
	return savedWatches, nil
}

// LoadWatches loads the sync.Map from disk, as sync.Map itself
// isn't able to be unmarshalled into
func LoadWatches(filePath string) (int, error) {
	var tmpMap map[string]Watch
	var loadedWatches int
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		logrus.Errorf("Error: %v", err)
		return 0, err
	}
	if err := json.Unmarshal(data, &tmpMap); err != nil {
		logrus.Error(err)
		return 0, err
	}
	for key, value := range tmpMap {
		watches.Store(key, &value)
		loadedWatches++
	}
	return loadedWatches, nil
}

// WatchWorker executes as a background routine checking registered API watches
func WatchWorker(s *discordgo.Session, sleepInterval time.Duration) {
	for {
		visited := make(map[string]string)
		logrus.Debug("Checking watches.")
		watches.Range(func(key, watchRaw interface{}) bool {
			var currentResult string
			var updateMsg string
			var updated bool
			var ok bool

			watch := watchRaw.(*Watch)
			visitedKey := watch.Pool + watch.Coin + watch.Wallet
			if currentResult, ok = visited[visitedKey]; ok {
				switch watch.Pool {
				case "Luxor":
					if watch.Wallet == "" {
						updated, updateMsg = luxor.IsStatsUpdated(watch.Coin, currentResult, watch.BaseValues)
					} else {
						updated, updateMsg = luxor.IsUserUpdated(watch.Coin, watch.Wallet, currentResult, watch.BaseValues)
					}
				}
			} else {
				switch watch.Pool {
				case "Luxor":
					if watch.Wallet == "" {
						currentResultObj, err := luxor.Stats(watch.Coin)
						if err != nil {
							logrus.Error(err)
							failedStatsRequests++
							logrus.Errorf("Failed to get stats object (%d times).", failedStatsRequests)
							return true
						}
						currentResultBytes, err := json.Marshal(currentResultObj)
						if err != nil {
							logrus.Error(err)
							logrus.Error("Failed to get stats watch json.")
							return true
						}
						currentResult = string(currentResultBytes)
						updated, updateMsg = luxor.IsStatsUpdated(watch.Coin, currentResult, watch.BaseValues)

					} else {
						currentResultObj, err := luxor.User(watch.Coin, watch.Wallet)
						if err != nil {
							logrus.Error(err)
							failedUserRequests++
							logrus.Errorf("Failed to get user object (%d times).", failedUserRequests)
							return true
						}
						currentResultBytes, err := json.Marshal(currentResultObj)
						if err != nil {
							logrus.Error(err)
							logrus.Error("Failed to get user watch json.")
							return true
						}
						currentResult = string(currentResultBytes)
						updated, updateMsg = luxor.IsUserUpdated(watch.Coin, watch.Wallet, currentResult, watch.BaseValues)
					}
					visited[visitedKey] = currentResult
				}
			}
			if updated {
				s.ChannelMessageSend(watch.ChannelID, updateMsg)
				watch.BaseValues = currentResult
				// TODO: If any tracked component updates, all are updated... even if other values didn't cross the threshold.
				watches.Store(key, watch)
			}
			return true
		})
		time.Sleep(sleepInterval)
	}
}
