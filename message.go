package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/not-inept/lifeguard/luxor"
)

func couldNotParse(s *discordgo.Session, channelID string) {
	s.ChannelMessageSend(channelID, "I couldn't get a response for that request.")
}

// MessageCreate will be called (due to AddHandler) every time a new
// message is created on any channel that the autenticated bot has access to.
func MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	if m.Author.ID == s.State.User.ID {
		return
	}

	if m.Content == "!help" {
		// TODO
	} else
	// All other commands for this bot are prefixed by '!lifeguard'
	if strings.HasPrefix(m.Content, "!lifeguard") {
		fields := strings.Fields(m.Content)
		if len(fields) == 1 { // !lifeguard
			s.ChannelMessageSend(m.ChannelID, "Hey, I'm lifeguard, I keep an eye on mining pools. If you want more info on me, try !lifeguard :). Thanks for letting me live on your server!")
			return
		}
		switch fields[1] {
		case "register-luxor": // Have us automatically monitor the Luxor API endpoints you care about
			if len(fields) == 3 {
				statsResult, err := luxor.Stats(fields[2])
				if err != nil {
					logrus.Error(err)
					couldNotParse(s, m.ChannelID)
					return
				}
				baseValues, err := json.Marshal(statsResult)
				if err != nil {
					logrus.Error(err)
					couldNotParse(s, m.ChannelID)
					return
				}
				watch := &Watch{
					OwnerID:    m.Author.ID,
					ChannelID:  m.ChannelID,
					Pool:       "Luxor",
					Coin:       strings.ToUpper(fields[2]),
					BaseValues: string(baseValues),
				}
				watches.Store(m.Author.ID+fields[2], watch)
			} else if len(fields) == 4 {
				userResult, err := luxor.User(fields[2], fields[3])
				if err != nil {
					logrus.Error(err)
					couldNotParse(s, m.ChannelID)
					return
				}
				baseValues, err := json.Marshal(userResult)
				if err != nil {
					logrus.Error(err)
					couldNotParse(s, m.ChannelID)
					return
				}
				watch := &Watch{
					OwnerID:    m.Author.ID,
					ChannelID:  m.ChannelID,
					Pool:       "Luxor",
					Coin:       strings.ToUpper(fields[2]),
					Wallet:     fields[3],
					BaseValues: string(baseValues),
				}
				watches.Store(m.Author.ID+fields[2]+fields[3], watch)
			}
			s.ChannelMessageSend(m.ChannelID, "I've added your requested watch!")
		case "luxor": // Get current status from Luxor API
			if len(fields) == 3 { // !lifeguard register-luxor <coin>
				statsResult, err := luxor.Stats(fields[2])
				if err != nil {
					logrus.Error(err)
					couldNotParse(s, m.ChannelID)
					return
				}
				s.ChannelMessageSend(m.ChannelID,
					fmt.Sprintf("Current status of %s:\n\tMiner count: %d\n\tHashrate: %f\n\tBlocks found: %d\n\tPrice: %s",
						strings.ToUpper(fields[2]),
						statsResult.MinerCount,
						statsResult.HashRate,
						statsResult.BlocksFound,
						statsResult.Price))
			} else if len(fields) == 4 { // !lifeguard register-luxor <coin> <wallet>
				userResult, err := luxor.User(fields[2], fields[3])
				if err != nil {
					logrus.Error(err)
					couldNotParse(s, m.ChannelID)
					return
				}
				s.ChannelMessageSend(m.ChannelID,
					fmt.Sprintf("Current status of %s:\n\tMiner count: %d\n\tHashrate: %f\n\tBlocks found: %d",
						strings.ToUpper(fields[3]),
						userResult.MinerCount,
						userResult.HashRate,
						userResult.BlocksFound))
			}
		}
	}
}
