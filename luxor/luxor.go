package luxor

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"strings"

	"github.com/google/go-cmp/cmp"
	"github.com/sirupsen/logrus"
)

var baseURL = "https://mining.luxor.tech/API/"

func request(args ...string) ([]byte, error) {
	reqResponse, _ := http.Get(baseURL + strings.Join(args, "/"))
	return ioutil.ReadAll(reqResponse.Body)
}

func isValidCoin(coin string) bool {
	switch coin {
	case
		"SC",
		"DCR",
		"LBC",
		"LOKI",
		"AION",
		"XMR",
		"ZEC",
		"ZEN",
		"SPACE":
		return true
	}
	return false
}

// Stats gets data from the Luxor <coin>/stats/ endpoint
func Stats(coin string) (StatsData, error) {
	coin = strings.ToUpper(coin)
	if !isValidCoin(coin) {
		return StatsData{}, errors.New("Invalid coin passed: " + coin)
	}
	logrus.Debug("Luxor /stats/ called with: ", coin)
	rawResponse, err := request(coin, "stats")
	if err != nil {
		return StatsData{}, err
	}
	var response statsResponse
	if err := json.Unmarshal(rawResponse, &response); err != nil {
		return StatsData{}, err
	}
	return StatsData{
		MinerCount:  response.TotalMiners,
		HashRate:    response.Hashrate,
		BlocksFound: response.BlocksFound,
		Price:       response.Price,
	}, nil
}

// User gets data from the Luxor <coin>/stats/<wallet> endpoint
func User(coin string, wallet string) (UserData, error) {
	coin = strings.ToUpper(coin)
	if !isValidCoin(coin) {
		return UserData{}, errors.New("Invalid coin passed: " + coin)
	}
	logrus.Debug("Luxor /user/ called with: ", coin, wallet)
	rawResponse, err := request(coin, "user", wallet)
	if err != nil {
		return UserData{}, err
	}
	var response userResponse
	if err := json.Unmarshal(rawResponse, &response); err != nil {
		logrus.Error("Raw response: ", string(rawResponse))
		return UserData{}, err
	}
	// Get list of miner names to keep track of
	miners := make([]string, len(response.Miners))
	for i, miner := range response.Miners {
		miners[i] = miner.Name
	}
	return UserData{
		MinerCount:    len(response.Miners),
		HashRate:      response.Hashrate1H,
		BlocksFound:   response.BlocksFound,
		Miners:        miners,
		StaleShares:   response.StaleSharesOneHour,
		InvalidShares: response.InvalidSharesOneHour,
		ValidShares:   response.ValidSharesOneHour,
		LastShareTime: response.LastShareTime,
	}, nil
}

// IsUserUpdated checks if the user api result is updated and builds a string describing the updates
func IsUserUpdated(coin string, wallet string, currentDataRaw string, baseValueRaw string) (bool, string) {
	// Marshall the stored base value
	var baseValue UserData
	json.Unmarshal([]byte(baseValueRaw), &baseValue)

	// Marshall stored current data
	var currentData UserData
	json.Unmarshal([]byte(currentDataRaw), &currentData)

	var updateDetails strings.Builder
	updated := false

	if !cmp.Equal(baseValue, currentData) {
		updateDetails.WriteString(fmt.Sprintf("Update for watch over %s/%s:", coin, wallet))
		if baseValue.MinerCount != currentData.MinerCount {
			miners := make(map[string]string)
			// This gets all miners in baseValue
			// and deletes those that are still in the currentData
			for _, miner := range baseValue.Miners {
				miners[miner] = "removed"
			}
			for _, miner := range currentData.Miners {
				if _, ok := miners[miner]; ok {
					// This miner is in both, so it can be ignored (is unchanged)
					delete(miners, miner)
				} else {
					miners[miner] = "added"
				}
			}

			updateDetails.WriteString(fmt.Sprintf("\n\tMiner count: %d -> %d", baseValue.MinerCount, currentData.MinerCount))
			for miner, status := range miners {
				updateDetails.WriteString(fmt.Sprintf("\n\t\tWorker %s was %s", miner, status))
			}

			updated = true
		}
		if baseValue.BlocksFound != currentData.BlocksFound {
			updateDetails.WriteString(fmt.Sprintf("\n\tBlocks found: %d -> %d", baseValue.BlocksFound, currentData.BlocksFound))
			updated = true
		}
		if math.Abs(currentData.HashRate-baseValue.HashRate) > (baseValue.HashRate * 0.1) {
			updateDetails.WriteString(fmt.Sprintf("\n\tHashrate: %f -> %f (%f%%)", baseValue.HashRate, currentData.HashRate, math.Round(math.Abs(currentData.HashRate-baseValue.HashRate)/baseValue.HashRate)))
			updated = true
		}
	}
	return updated, updateDetails.String()
}

// IsStatsUpdated checks if the stats api result is updated and builds a string describing the updates
func IsStatsUpdated(coin string, currentDataRaw string, baseValueRaw string) (bool, string) {
	// Marshall the stored base value
	var baseValue StatsData
	json.Unmarshal([]byte(baseValueRaw), &baseValue)

	// Marshall stored current data
	var currentData StatsData
	json.Unmarshal([]byte(currentDataRaw), &currentData)

	var updateDetails strings.Builder
	updated := false
	logrus.Infof("Base: %d, %d, %f", baseValue.BlocksFound, baseValue.MinerCount, baseValue.HashRate)
	logrus.Infof("Curr: %d, %d, %f", currentData.BlocksFound, currentData.MinerCount, currentData.HashRate)

	if !cmp.Equal(baseValue, currentData) {
		updateDetails.WriteString(fmt.Sprintf("Update for watch over %s:", coin))
		if math.Abs(float64(baseValue.MinerCount-currentData.MinerCount)) > (float64(baseValue.MinerCount) * 0.1) {
			updateDetails.WriteString(fmt.Sprintf("\n\tMiner count: %d -> %d", baseValue.MinerCount, currentData.MinerCount))
			updated = true
		}
		if baseValue.BlocksFound != currentData.BlocksFound {
			updateDetails.WriteString(fmt.Sprintf("\n\tBlocks found: %d -> %d.", baseValue.BlocksFound, currentData.BlocksFound))
			updated = true
		}
		if math.Abs(currentData.HashRate-baseValue.HashRate) > (baseValue.HashRate * 0.1) {
			updateDetails.WriteString(fmt.Sprintf("\n\tHashrate: %f -> %f (%f%%)",
				baseValue.HashRate,
				currentData.HashRate,
				math.Round(math.Abs((currentData.HashRate-baseValue.HashRate)/baseValue.HashRate)*100)))
			updated = true
		}
	}
	return updated, updateDetails.String()
}
