package luxor

import "time"

// StatsData contains information we care about from the /stats/ api response
type StatsData struct {
	MinerCount  int
	HashRate    float64
	BlocksFound int
	Price       string
}

// UserData contains information we care about from the /user/ api response
type UserData struct {
	MinerCount    int
	HashRate      float64
	BlocksFound   int
	Miners        []string
	StaleShares   int
	InvalidShares int
	ValidShares   int
	LastShareTime int
}

// Response from the Luxor /<coin>/stats/ request
type statsResponse struct {
	Price       string  `json:"price"`
	Hashrate    float64 `json:"hashrate"`
	BlocksFound int     `json:"blocksFound"`
	TotalMiners int     `json:"totalMiners"`
	GlobalStats []struct {
		Time              time.Time `json:"time"`
		NetworkHashrate   int       `json:"network_hashrate"`
		PoolHashrate      float64   `json:"pool_hashrate"`
		Workers           int       `json:"workers"`
		NetworkDifficulty int64     `json:"network_difficulty"`
		CoinPrice         string    `json:"coin_price"`
		BtcPrice          string    `json:"btc_price"`
	} `json:"globalStats"`
}

// Response from the Luxor /<coin>/user/<wallet> request
type userResponse struct {
	Address                 string      `json:"address"`
	Balance                 float64     `json:"balance"`
	EstimatedBalance        interface{} `json:"estimated_balance"`
	Hashrate1H              float64     `json:"hashrate_1h"`
	Hashrate24H             float64     `json:"hashrate_24h"`
	ValidSharesFiveMin      int         `json:"valid_shares_five_min"`
	ValidSharesFifteenMin   int         `json:"valid_shares_fifteen_min"`
	ValidSharesOneHour      int         `json:"valid_shares_one_hour"`
	ValidSharesSixHour      int         `json:"valid_shares_six_hour"`
	ValidSharesOneDay       int         `json:"valid_shares_one_day"`
	InvalidSharesFiveMin    int         `json:"invalid_shares_five_min"`
	InvalidSharesFifteenMin int         `json:"invalid_shares_fifteen_min"`
	InvalidSharesOneHour    int         `json:"invalid_shares_one_hour"`
	InvalidSharesSixHour    int         `json:"invalid_shares_six_hour"`
	InvalidSharesOneDay     int         `json:"invalid_shares_one_day"`
	StaleSharesFiveMin      int         `json:"stale_shares_five_min"`
	StaleSharesFifteenMin   int         `json:"stale_shares_fifteen_min"`
	StaleSharesOneHour      int         `json:"stale_shares_one_hour"`
	StaleSharesSixHour      int         `json:"stale_shares_six_hour"`
	StaleSharesOneDay       int         `json:"stale_shares_one_day"`
	PayoutsFiveMin          float64     `json:"payouts_five_min"`
	PayoutsFifteenMin       float64     `json:"payouts_fifteen_min"`
	PayoutsOneHour          float64     `json:"payouts_one_hour"`
	PayoutsSixHour          float64     `json:"payouts_six_hour"`
	PayoutsOneDay           float64     `json:"payouts_one_day"`
	TotalPayouts            float64     `json:"total_payouts"`
	BlocksFound             int         `json:"blocks_found"`
	LastShareTime           int         `json:"last_share_time"`
	Payouts                 []struct {
		Username string    `json:"username"`
		Amount   string    `json:"amount"`
		TxID     string    `json:"tx_id"`
		Time     time.Time `json:"time"`
	} `json:"payouts"`
	Miners []struct {
		Name               string  `json:"name"`
		Affinity           string  `json:"affinity"`
		MinerType          string  `json:"miner_type"`
		LastShareTime      int     `json:"last_share_time"`
		TotalShares        float64 `json:"total_shares"`
		HashrateFiveMin    float64 `json:"hashrate_five_min"`
		HashrateFifteenMin float64 `json:"hashrate_fifteen_min"`
		HashrateOneHour    float64 `json:"hashrate_one_hour"`
		HashrateSixHour    float64 `json:"hashrate_six_hour"`
		HashrateOneDay     float64 `json:"hashrate_one_day"`
		StaleFiveMin       int     `json:"stale_five_min"`
		StaleFifteenMin    int     `json:"stale_fifteen_min"`
		StaleOneHour       int     `json:"stale_one_hour"`
		StaleSixHour       int     `json:"stale_six_hour"`
		StaleOneDay        int     `json:"stale_one_day"`
		RevenueFiveMin     string  `json:"revenue_five_min"`
		RevenueFifteenMin  string  `json:"revenue_fifteen_min"`
		RevenueOneHour     string  `json:"revenue_one_hour"`
		RevenueSixHour     string  `json:"revenue_six_hour"`
		RevenueOneDay      string  `json:"revenue_one_day"`
	} `json:"miners"`
	Hashrate []struct {
		Time     int     `json:"time"`
		Hashrate float64 `json:"hashrate"`
	} `json:"hashrate"`
}
